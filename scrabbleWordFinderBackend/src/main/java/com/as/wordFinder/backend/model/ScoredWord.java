package com.as.wordFinder.backend.model;

public class ScoredWord{

    private final String word;
    private final int score;

    public ScoredWord(String word, int score) {
        this.word = word;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public String getWord() {
        return word;
    }
}
