package com.as.wordFinder.backend.controller;

import com.as.wordFinder.backend.model.ScoredWord;
import com.as.wordFinder.backend.search.WordSearcher;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/scrabbleWordFinder")
public class WordFinderBackendController {

    private WordSearcher wordSearcher;

    public WordFinderBackendController() {
        this.wordSearcher = new WordSearcher();
    }

    @PostMapping(value = "/search")
    public List<ScoredWord> searchWords(@RequestParam String ownedLetters,
                                 @RequestParam String boardLetter){
        return wordSearcher.search(ownedLetters,boardLetter);
    }

    @RequestMapping("/allWords")
    public List<String> getAllWords(@RequestParam String language){
        return null;
    }

    @RequestMapping("/allWordsWithScores")
    public List<String> getAllWordsWithScores(@RequestParam String language){
        return null;
    }
}
