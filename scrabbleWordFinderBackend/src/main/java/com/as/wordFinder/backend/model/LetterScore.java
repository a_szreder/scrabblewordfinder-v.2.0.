package com.as.wordFinder.backend.model;

public enum LetterScore {
    a(1), ą(5), b(3), c(2), ć(6), d(2), e(1), ę(5),
    f(5), g(3), h(3), i(1), j(3), k(2), l(2), ł(3),
    m(2), n(1), ń(7), o(1), ó(5), p(2), q(0), r(1),
    s(1), ś(5), t(2), u(3), w(1), v(0), x(0), y(2),
    z(1), ż(5), ź(9);

    private int letterScore;

    LetterScore(int score) {
        letterScore = score;
    }

    public int getLetterScore() {
        return letterScore;
    }

    public static int getLetterScore(char letter) {
        return LetterScore.valueOf(String.valueOf(letter)).getLetterScore();
    }
}
