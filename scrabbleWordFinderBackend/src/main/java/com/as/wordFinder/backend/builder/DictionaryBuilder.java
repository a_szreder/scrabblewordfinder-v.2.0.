package com.as.wordFinder.backend.builder;

import com.as.wordFinder.backend.model.LetterScore;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DictionaryBuilder {
    private List<String> wordList;
    private Map<String,Integer> dictionary;

    public DictionaryBuilder() {
        this.wordList = new ArrayList<>();
        this.dictionary = new HashMap<>();
    }

    public void buildDictionary(){
        buildDictionary("PL");
    }

    private void buildDictionary(String language){
        readDictionaryFile(language);
        if(wordList.size()>0){
            for(String word: wordList){
                dictionary.put(word, getScore(word));
            }
        }
    }

    public List<String> getWordList() {
        return wordList;
    }

    public Map<String, Integer> getDictionary() {
        return dictionary;
    }

    private int getScore(String word){
        int score = 0;
        for(char letter: word.toCharArray()){
            score += LetterScore.getLetterScore(letter);
        }
        return score;
    }

    private void readDictionaryFile(String language) {
        InputStream is = DictionaryBuilder.class.getResourceAsStream("/dictionaries/"+language+".txt");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String st;
            while ((st = br.readLine()) != null)
                wordList.add(st);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

