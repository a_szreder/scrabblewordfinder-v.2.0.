package com.as.wordFinder.backend.search;

import com.as.wordFinder.backend.builder.DictionaryBuilder;
import com.as.wordFinder.backend.model.ScoredWord;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordSearcher {
    private final static DictionaryBuilder dictionaryBuilder = new DictionaryBuilder();

    public WordSearcher() {
        dictionaryBuilder.buildDictionary();
    }

    public List<ScoredWord> search(String ownedLetters, String boarLetters) {
        Map<Character, Long> frequencies = ownedLetters.concat(boarLetters).chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(Function.identity(),
                        Collectors.counting()));

        return dictionaryBuilder.getDictionary()
                .keySet()
                .stream()
                .filter(key -> ownedLetters.concat(boarLetters).length() >= key.length())
                .filter(key -> key.chars().mapToObj(c -> (char) c)
                        .map(letter -> ownedLetters.concat(boarLetters)
                                .contains(String.valueOf(letter)))
                        .filter(contain -> !contain)
                        .collect(Collectors.toList())
                        .isEmpty())
                .filter(word -> word.contains(boarLetters))
                .filter(key -> key.chars()
                        .mapToObj(c -> (char) c)
                        .map(letter -> frequencies.get(letter) >= (key.chars()
                                .filter(c -> c == letter)
                                .count()))
                        .filter(contain -> !contain)
                        .collect(Collectors.toList())
                        .isEmpty())
                .filter(key -> key.length() > boarLetters.length())
                .map(key -> new ScoredWord(key, dictionaryBuilder.getDictionary().get(key)))
                .sorted(Comparator.comparing(ScoredWord::getScore).reversed())
                .collect(Collectors.toList());
    }
}
