'use strict';

angular
    .module('myApp')
    .service('ReguestHandler', ['$q','$http', '$httpParamSerializerJQLike',
    function ($q, $http, $httpParamSerializerJQLike) {
        var me = this;

        me.getWordRequest = function(ownedLetter, boardLetter){
            var defer = $q.defer();
            var data = {
                ownedLetters: ownedLetter,
                boardLetter: boardLetter
            };
            $http({
                method: 'POST',
                url: 'http://localhost:8090/scrabbleWordFinder/search',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $httpParamSerializerJQLike(data)
            }).then(function (response){
                defer.resolve(response['data']);
            });

            return defer.promise;
        }
    }
]);