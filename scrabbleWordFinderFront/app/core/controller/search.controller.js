'use strict';

angular
    .module('myApp')
    .controller('SearchController', ['$scope', 'ReguestHandler',
        function ($scope, ReguestHandler) {
            $scope.machedWords = [];

            $scope.search = function (ownedLetter, boardLetter) {
                return ReguestHandler.getWordRequest(ownedLetter, boardLetter)
                    .then(function (foundedWords) {
                        $scope.machedWords =[];
                        if(foundedWords.length >=1){
                            foundedWords.forEach(function(word){
                                $scope.machedWords.push(word);
                            })
                        }
                    });
            }
        }
    ]);
